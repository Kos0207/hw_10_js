let names = document.querySelectorAll('.tabs-title');
names.forEach(name => name.addEventListener('click', function () {
	const tab = name.dataset.tab
	document.querySelector(".tabs-title.active").classList.remove("active")
	name.classList.add("active")

	document.querySelector(".tabs-content li.active").classList.remove("active")
	document.getElementById(tab).classList.add("active")
}))


